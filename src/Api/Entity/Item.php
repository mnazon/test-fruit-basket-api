<?php

namespace App\Api\Entity;

use App\Api\Enum\ItemType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\ItemRepository")
 * @ORM\Table(name="item")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"api", "list"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=15)
     *
     * @Groups({"api", "list"})
     */
    private string $type;

    /**
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     *
     * @Groups({"api", "list"})
     */
    private int $weight;

    /**
     * @ORM\ManyToOne(targetEntity="App\Api\Entity\Basket", inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private Basket $basket;

    public function __construct(Basket $basket, ItemType $itemType, int $weight)
    {
        $this->basket = $basket;
        $this->type = $itemType->getValue();
        $this->weight = $weight;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ItemType
    {
        return new ItemType($this->type);
    }

    public function getWeight(): int
    {
        return $this->weight;
    }
}
