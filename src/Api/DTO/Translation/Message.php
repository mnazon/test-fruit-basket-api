<?php

declare(strict_types=1);

namespace App\Api\DTO\Translation;

class Message
{
    private string $messageTemplate;

    private array $parameters;

    private ?string $domain;

    public function __construct(string $messageTemplate, array $parameters = [], string $domain = null)
    {
        $this->messageTemplate = $messageTemplate;
        $this->parameters = $parameters;
        $this->domain = $domain;
    }

    public function getMessageTemplate(): string
    {
        return $this->messageTemplate;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }
}
