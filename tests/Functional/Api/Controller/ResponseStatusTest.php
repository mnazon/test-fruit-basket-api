<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api\Controller;

use App\Tests\Functional\BaseApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class ResponseStatusTest extends BaseApiTestCase
{
    /**
     * @dataProvider dataProvider
     *
     * @param string $method
     * @param string $url
     * @param int    $expectedStatus
     */
    public function testResponseStatusCode(string $method, string $url, int $expectedStatus): void
    {
        $this->client->request($method, $url);

        $this->getResponseAsserter()
            ->assertStatusEquals($expectedStatus, $this->client->getResponse());
    }

    public function dataProvider(): iterable
    {
        yield ['GET', '/', Response::HTTP_NOT_FOUND];
        yield ['GET', '/api/doc', Response::HTTP_OK];
    }
}
