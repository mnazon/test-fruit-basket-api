<?php

namespace App\Api\Repository;

use App\Api\Entity\Basket;
use App\Api\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function findByBasket(Basket $basket): array
    {
        return $this->findBy(['basket' => $basket]);
    }

    public function removeItemsByBasketAndIds(Basket $basket, array $ids)
    {
        return $this->createQueryBuilder('i')
            ->delete()
            ->where('i.basket = :basket')
            ->andWhere('i.id IN(:ids)')
            ->setParameters([
                'basket' => $basket,
                'ids' => $ids,
            ])
            ->getQuery()
            ->execute();
    }
}
