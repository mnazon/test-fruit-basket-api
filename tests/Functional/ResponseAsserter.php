<?php

namespace App\Tests\Functional;

use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ResponseAsserter
{
    private Assert $assert;

    private ?PropertyAccessor $accessor = null;

    public function __construct(Assert $assert)
    {
        $this->assert = $assert;
    }

    /**
     * @expectedException
     *
     * @param Response $response
     *
     * @return $this
     */
    public function assertStatusOk(Response $response): self
    {
        $this->assertStatusEquals(Response::HTTP_OK, $response);

        return $this;
    }

    /**
     * @expectedException
     *
     * @param int      $expectedStatus
     * @param Response $response
     *
     * @return $this
     */
    public function assertStatusEquals(int $expectedStatus, Response $response): self
    {
        $this->assert::assertEquals($expectedStatus, $response->getStatusCode(), $response->getContent());

        return $this;
    }

    /**
     * Asserts the array of property names are in the JSON response.
     * @expectedException

     * @param Response $response
     * @param array    $expectedProperties
     *
     * @return $this
     */
    public function assertResponsePropertiesExist(Response $response, array $expectedProperties): self
    {
        foreach ($expectedProperties as $propertyPath) {
            // this will blow up if the property doesn't exist
            $this->readResponseProperty($response, $propertyPath);
        }

        return $this;
    }

    /**
     * Asserts the response JSON property equals the given value.
     *
     * @expectedException
     *
     * @param Response $response
     * @param string   $propertyPath  e.g. firstName, battles[0].programmer.username
     * @param mixed    $expectedValue
     *
     * @return $this
     */
    public function assertResponsePropertyEquals(Response $response, string $propertyPath, $expectedValue): self
    {
        $actual = $this->readResponseProperty($response, $propertyPath);
        $this->assert::assertEquals(
            $expectedValue,
            $actual,
            sprintf(
                'Property "%s": Expected "%s" but response was "%s"',
                $propertyPath,
                $expectedValue,
                var_export($actual, true)
            )
        );

        return $this;
    }

    public function assertResponseContentEquals(Response $response, string $expectedValue): self
    {
        $content = $response->getContent();
        $this->assert::assertEquals(
            $expectedValue,
            $content,
            sprintf('Expected "%s" but response was "%s"', $expectedValue, $content)
        );

        return $this;
    }

    /**
     * Reads a JSON response property and returns the value.
     *
     * This will explode if the value does not exist
     *
     * @expectedException
     *
     * @param Response $response
     * @param string   $propertyPath e.g. firstName, battles[0].programmer.username
     *
     * @return mixed
     */
    private function readResponseProperty(Response $response, string $propertyPath)
    {
        if (null === $this->accessor) {
            $this->accessor = PropertyAccess::createPropertyAccessor();
        }

        $data = json_decode((string) $response->getContent());
        if (null === $data) {
            throw new \Exception(sprintf(
                'Cannot read property "%s" - the response is invalid',
                $propertyPath
            ));
        }
        try {
            return $this->accessor->getValue($data, $propertyPath);
        } catch (AccessException $e) {
            // it could be a stdClass or an array of stdClass
            $values = is_array($data) ? $data : get_object_vars($data);
            throw new AccessException(sprintf(
                'Error reading property "%s" from available keys (%s) of object(%s)',
                $propertyPath,
                implode(', ', array_keys($values)),
                var_export($values, true)
            ), 0, $e);
        }
    }
}
