<?php

declare(strict_types=1);

namespace App\Api\Request\ParamConverter;

use App\Api\Entity\Basket;
use App\Api\Repository\BasketRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BasketParamConverter implements ParamConverterInterface
{
    private const BASKET_ATTRIBUTE_ID = 'basketId';

    private BasketRepository $repository;

    public function __construct(BasketRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     *
     * @throws NotFoundHttpException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $paramName = $configuration->getName();
        $basket = $this->find($request, $paramName);

        if (!$basket) {
            throw new NotFoundHttpException('Basket not found');
        }

        $request->attributes->set($paramName, $basket);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration): bool
    {
        if (null === $configuration->getClass()) {
            return false;
        }

        return Basket::class === $configuration->getClass();
    }

    private function find(Request $request, $paramName): ?Basket
    {
        $id = $this->getIdentifier($request, $paramName);

        if (null === $id) {
            return null;
        }

        return $this->repository->find($id);
    }

    private function getIdentifier(Request $request, string $paramName): ?int
    {
        if ($request->attributes->has(self::BASKET_ATTRIBUTE_ID)) {
            return $request->attributes->getInt(self::BASKET_ATTRIBUTE_ID);
        } elseif ($request->attributes->has($paramName)) {
            return $request->attributes->getInt($paramName);
        }

        return null;
    }
}
