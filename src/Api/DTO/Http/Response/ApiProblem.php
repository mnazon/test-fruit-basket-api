<?php

declare(strict_types=1);

namespace App\Api\DTO\Http\Response;

use App\Api\DTO\Translation\Message;

class ApiProblem
{
    private string $type;

    private string $title;

    private ?Message $detail;

    private ?string $status;

    /**
     * @var ApiError[]
     */
    private array $errors = [];

    public function __construct(string $type, string $title, Message $detail = null, string $status = null)
    {
        $this->type = $type;
        $this->title = $title;
        $this->detail = $detail;
        $this->status = $status;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function addError(ApiError $apiError)
    {
        $this->errors[] = $apiError;
    }

    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDetail(): ?Message
    {
        return $this->detail;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }
}
