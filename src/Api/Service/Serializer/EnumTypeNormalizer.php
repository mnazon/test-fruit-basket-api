<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use MyCLabs\Enum\Enum;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class EnumTypeNormalizer implements NormalizerInterface
{
    public function normalize($object, string $format = null, array $context = [])
    {
        /* @var $object Enum */
        return $object->getValue();
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof Enum;
    }
}
