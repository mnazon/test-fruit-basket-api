<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api\Controller;

use App\Api\Entity\Item;
use App\Tests\Functional\BaseApiTestCase;

/**
 * @todo bad request tests
 */
class ItemControllerTest extends BaseApiTestCase
{
    private const BASE_URL_FORMAT = '/api/v1/baskets/%s/items';

    private const ITEM_STRUCTURE = [
        'id',
        'type',
        'weight',
    ];

    public function testSuccessGetItems()
    {
        $basket = $this->loadAppFixtures()->getBasket();
        $url = sprintf(self::BASE_URL_FORMAT, $basket->getId());
        $this->client->request('GET', $url);
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusOk($response)
            ->assertResponsePropertiesExist(
                $response,
                $this->prepareCollectionStructure(self::ITEM_STRUCTURE, 1)
            );
    }

    public function testSuccessAddItems()
    {
        $items = [
            [
                'type' => 'apple',
                'weight' => 100,
            ],
            [
                'type' => 'watermelon',
                'weight' => 100,
            ],
            [
                'type' => 'orange',
                'weight' => 100,
            ],
        ];

        $data = ['items' => $items];
        $basket = $this->loadAppFixtures()->getBasket();
        $url = sprintf(self::BASE_URL_FORMAT, $basket->getId());
        $this->client->request('POST', $url, [], [], self::$defaultHeaders, json_encode($data));
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusOk($response)
            ->assertResponsePropertiesExist(
                $response,
                $this->prepareCollectionStructure(self::ITEM_STRUCTURE, 3)
            );
    }

    public function testSuccessRemoveItems()
    {
        $basket = $this->loadAppFixtures()->getBasket();
        $ids = [];
        /** @var Item $item */
        foreach ($basket->getItems() as $item) {
            $ids[] = $item->getId();
        }

        $this->assertNotEmpty($ids);

        $data = ['items' => $ids];
        $url = sprintf(self::BASE_URL_FORMAT, $basket->getId());
        $this->client->request('DELETE', $url, [], [], self::$defaultHeaders, json_encode($data));
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusOk($response)
            ->assertResponseContentEquals($response, 'null');

        $this->client->request('GET', $url);
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusOk($response)
            ->assertResponseContentEquals($response, '[]');
    }
}
