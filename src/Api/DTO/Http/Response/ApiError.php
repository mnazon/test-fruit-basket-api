<?php

declare(strict_types=1);

namespace App\Api\DTO\Http\Response;

use App\Api\DTO\Translation\Message;

class ApiError
{
    private string $code;

    private Message $title;

    private ?string $propertyPath;

    public function __construct(string $code, Message $title, ?string $propertyPath = null)
    {
        $this->code = $code;
        $this->title = $title;
        $this->propertyPath = $propertyPath;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getTitle(): Message
    {
        return $this->title;
    }

    public function getPropertyPath(): ?string
    {
        return $this->propertyPath;
    }
}
