<?php

declare(strict_types=1);

namespace App\Api\ArgumentResolver;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\Enum\ApiErrorCode;
use App\Api\Exception\ApiErrorCodeException;
use App\Api\Exception\ConstraintViolationListException;
use App\Api\Repository\BasketRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Exception\MissingConstructorArgumentsException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BodyDataArgumentResolver implements ArgumentValueResolverInterface
{
    private SerializerInterface $serializer;

    private ValidatorInterface $validator;

    private BasketRepository $basketRepo;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        BasketRepository $basketRepo
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->basketRepo = $basketRepo;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return in_array($request->getMethod(), ['POST', 'PUT', 'DELETE'])
            && class_exists($argument->getType())
            && in_array(ArgumentResolvableInterface::class, class_implements($argument->getType()), true)
        ;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $data = $request->getContent();
        try {
            $object = $this->serializer->deserialize($data, $argument->getType(), 'json');
        } catch (NotNormalizableValueException | MissingConstructorArgumentsException | NotEncodableValueException $e) {
            $apiErrorCode = ApiErrorCode::BAD_REQUEST_DATA()->setTitle($e->getMessage());
            throw new ApiErrorCodeException($apiErrorCode);
        }

        $violations = $this->validator->validate($object);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }

        yield $object;
    }
}
