<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\Http\Response\ApiError;
use App\Api\DTO\Http\Response\ApiProblem;
use App\Api\DTO\Translation\Message;
use App\Api\Enum\ApiErrorCode;

class ApiProblemBuilder
{
    private ApiErrorCode $apiErrorCode;

    public function setApiErrorCode(ApiErrorCode $apiErrorCode)
    {
        $this->apiErrorCode = $apiErrorCode;
    }

    public function build(): ApiProblem
    {
        $apiError = new ApiError(
            $this->apiErrorCode->getValue(),
            new Message($this->apiErrorCode->getTitle())
        );

        $apiProblem = new ApiProblem(
            $this->apiErrorCode->getType(),
            $this->apiErrorCode->getTitle()
        );
        $apiProblem->addError($apiError);

        return $apiProblem;
    }
}
