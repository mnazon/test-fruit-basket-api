<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController extends AbstractController
{
    public const RESPONSE_DEFAULT_GROUP = 'api';
    public const RESPONSE_GROUP_LIST = 'list';

    private array $defaultResponseContext = [
        'groups' => [self::RESPONSE_DEFAULT_GROUP],
    ];

    public function serialize($data, $status = Response::HTTP_OK, array $context = [], array $headers = [])
    {
        $context = array_merge($this->defaultResponseContext, $context);

        return $this->json($data, $status, $headers, $context);
    }
}
