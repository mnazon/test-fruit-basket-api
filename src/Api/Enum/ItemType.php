<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

class ItemType extends Enum
{
    public const APPLE = 'apple';
    public const ORANGE = 'orange';
    public const WATERMELON = 'watermelon';
}
