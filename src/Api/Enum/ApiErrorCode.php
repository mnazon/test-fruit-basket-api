<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static self INTERNAL_ERROR()
 * @method static self BAD_REQUEST_DATA()
 */
class ApiErrorCode extends Enum
{
    public const INTERNAL_ERROR = 'internal_error';
    public const BAD_REQUEST_DATA = 'bad_request_data';

    private string $title;

    private static array $codeToTitle = [
        self::BAD_REQUEST_DATA => 'Bad request data',
    ];

    /**
     * @todo wiki with api errors
     *
     * @return string
     */
    public static function getType(): string
    {
        return 'api-error';
    }

    public function getTitle(): string
    {
        if (!empty($this->title)) {
            return $this->title;
        }

        return self::$codeToTitle[$this->getValue()] ?? 'Validation error';
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
