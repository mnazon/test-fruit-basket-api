<?php

namespace App\DataFixtures;

use App\Api\Entity\Basket;
use App\Api\Entity\Item;
use App\Api\Enum\ItemType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private Basket $basket;

    public function load(ObjectManager $manager)
    {
        $basket = new Basket('test', 400);
        $item = new Item($basket, ItemType::APPLE(),100);
        $basket->addItem($item);

        $manager->persist($basket);
        $manager->flush();

        $this->basket = $basket;
    }

    public function getBasket(): Basket
    {
        return $this->basket;
    }
}
