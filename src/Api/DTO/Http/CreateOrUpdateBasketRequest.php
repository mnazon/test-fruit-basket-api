<?php

declare(strict_types=1);

namespace App\Api\DTO\Http;

use App\Api\DTO\ArgumentResolvableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

class CreateOrUpdateBasketRequest implements ArgumentResolvableInterface
{
    /**
     * @Assert\NotBlank()
     *
     * @Groups("api")
     *
     * @var string
     */
    private string $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="1", max="1000")
     *
     * @Groups("api")
     *
     * @var int
     */
    private int $maxWeight;

    public function __construct(string $name, int $maxWeight)
    {
        $this->name = $name;
        $this->maxWeight = $maxWeight;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMaxWeight(): int
    {
        return $this->maxWeight;
    }
}
