<?php

declare(strict_types=1);

namespace App\Api\Exception;

use App\Api\DTO\Http\Response\ApiProblem;
use App\Api\Enum\ApiErrorCode;
use App\Api\Service\ApiProblemBuilder;
use Throwable;

class ApiErrorCodeException extends \Exception implements ApiProblemExceptionInterface
{

    private ApiProblem $apiProblem;

    private int $statusCode;

    private array $headers;

    public function __construct(
        ApiErrorCode $apiError,
        int $statusCode = 400,
        array $headers = [],
        string $message = '',
        int $code = 0,
        Throwable $previous = null
    ) {
        $apiProblemBuilder = new ApiProblemBuilder();
        $apiProblemBuilder->setApiErrorCode($apiError);
        $this->apiProblem = $apiProblemBuilder->build();

        parent::__construct($message, $code, $previous);
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function getApiProblem(): ApiProblem
    {
        return $this->apiProblem;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
