<?php

declare(strict_types=1);

namespace App\Api\EventListener;

use App\Api\DTO\Http\Response\ApiError;
use App\Api\DTO\Http\Response\ApiProblem;
use App\Api\DTO\Translation\Message;
use App\Api\Enum\ApiErrorCode;
use App\Api\Exception\ApiProblemExceptionInterface;
use App\Api\Exception\ConstraintViolationListException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ApiExceptionListener
{
    private SerializerInterface $serializer;

    private LoggerInterface $logger;

    public function __construct(SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $e = $event->getThrowable();
        if ($e instanceof ConstraintViolationListException) {
            $apiProblem = $e->getConstraintViolationList();
        } elseif ($e instanceof ApiProblemExceptionInterface) {
            $apiProblem = $e->getApiProblem();
        } else {
            if ($e instanceof HttpExceptionInterface) {
                $title = $e->getMessage();
                $apiError = new ApiError(ApiErrorCode::BAD_REQUEST_DATA, new Message($title));
            } else {
                $title = 'Internal error';
                $apiError = new ApiError(ApiErrorCode::INTERNAL_ERROR, new Message($title));
            }

            $apiProblem = new ApiProblem(ApiErrorCode::getType(), $title);
            $apiProblem->addError($apiError);
        }

        $isHandledException = $e instanceof HttpExceptionInterface;
        if ($isHandledException) {
            $statusCode = $e->getStatusCode();
            $headers = $e->getHeaders();
        } else {
            $this->logger->error('Not handled exception', ['exception' => $e]);
            $statusCode = 500;
            $headers = [];
            throw $e;
        }

        $data = $this->serializer->serialize($apiProblem, 'json');
        $response = new JsonResponse($data, $statusCode, $headers, true);

        $response->headers->set('Content-Type', 'application/problem+json');
        $event->setResponse($response);
    }
}
