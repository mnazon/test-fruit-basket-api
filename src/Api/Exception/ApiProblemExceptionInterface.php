<?php

declare(strict_types=1);

namespace App\Api\Exception;

use App\Api\DTO\Http\Response\ApiProblem;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

interface ApiProblemExceptionInterface extends HttpExceptionInterface
{
    public function getApiProblem(): ApiProblem;
}
