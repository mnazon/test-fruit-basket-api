<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\Http\AddItemsToBasketRequest;
use App\Api\DTO\Http\RemoveItemsFromBasketRequest;
use App\Api\Entity\Basket;
use App\Api\Entity\Item;
use App\Api\Exception\ConstraintViolationListException;
use App\Api\Repository\ItemRepository;
use App\Api\Service\BasketManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/baskets/{basketId}/items")
 *
 * @SWG\Tag(name="Items")
 */
class BasketItemController extends AbstractApiController
{
    /**
     * @todo paging
     *
     * @Route("", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns items",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Item::class, groups={"list"}))
     *     )
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Basket not found",
     * ),
     *
     * @param Basket         $basket
     * @param ItemRepository $repository
     *
     * @return JsonResponse
     */
    public function list(Basket $basket, ItemRepository $repository)
    {
        $items = $repository->findByBasket($basket);

        return $this->serialize($items, Response::HTTP_OK, ['groups' => self::RESPONSE_GROUP_LIST]);
    }

    /**
     * @Route("", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @Model(type=AddItemsToBasketRequest::class, groups={"list"})
     * ),
     *
     * @SWG\Response(
     *     response=200,
     *     description="if operation success",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Item::class, groups={"list"}))
     *     )
     * ),
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Basket not found",
     * ),
     *
     * @param Basket                  $basket
     * @param AddItemsToBasketRequest $request
     * @param BasketManager           $manager
     * @param ValidatorInterface      $validator
     *
     * @return JsonResponse
     *
     * @throws ConstraintViolationListException
     */
    public function addItemsToBasket(
        Basket $basket,
        AddItemsToBasketRequest $request,
        BasketManager $manager,
        ValidatorInterface $validator
    ) {
        $constraint = new Assert\LessThanOrEqual([
            'value' => $basket->getMaxWeight(),
            'message' => 'Total weight should be less than or equal to {{ compared_value }}.',
        ]);
        $totalWeight = $request->getWeight() + $basket->getWeight();

        $violations = $validator->validate($totalWeight, $constraint);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }

        $items = $manager->addItemsToBasket($basket, $request);

        return $this->serialize($items, Response::HTTP_OK, ['groups' => self::RESPONSE_GROUP_LIST]);
    }

    /**
     * @Route("", methods={"DELETE"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @Model(type=RemoveItemsFromBasketRequest::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response="200",
     *     description="if operation success"
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Basket not found",
     * ),
     *
     * @param Basket                       $basket
     * @param RemoveItemsFromBasketRequest $request
     * @param ItemRepository               $itemRepository
     *
     * @return JsonResponse
     */
    public function removeItemsFromBasket(
        Basket $basket,
        RemoveItemsFromBasketRequest $request,
        ItemRepository $itemRepository
    ) {
        $ids = $request->getItems();
        $removedRows = $itemRepository->removeItemsByBasketAndIds($basket, $ids);

        return $this->serialize(null);
    }
}
