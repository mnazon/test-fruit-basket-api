<?php

declare(strict_types=1);

namespace App\Api\DTO\Http;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\ItemData;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

class AddItemsToBasketRequest implements ArgumentResolvableInterface
{
    /**
     * @Assert\NotNull()
     * @Assert\Valid()
     *
     * @Groups("list")
     *
     * @var ItemData[]
     */
    private array $items;

    public function __construct(array $items)
    {
        foreach ($items as $item) {
            if (!$item instanceof ItemData) {
                throw new \InvalidArgumentException('Wrong instance of item setup');
            }
        }

        $this->items = $items;
    }

    /**
     * @return ItemData[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getWeight(): int
    {
        $itemsWeight = 0;
        /** @var ItemData $item */
        foreach ($this->getItems() as $item) {
            $itemsWeight += $item->getWeight();
        }

        return  $itemsWeight;
    }
}
