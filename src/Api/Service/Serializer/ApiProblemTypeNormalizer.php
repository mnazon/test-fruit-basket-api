<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use App\Api\DTO\Http\Response\ApiError;
use App\Api\DTO\Http\Response\ApiProblem;
use App\Api\DTO\Translation\Message;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApiProblemTypeNormalizer implements NormalizerInterface
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        /** @var ApiProblem $object */
        $data = [
            'type' => $object->getType(),
            'title' => $object->getTitle(),
        ];
        if ($object->getDetail()) {
            $data['detail'] = $this->translate($object->getDetail());
        }

        if ($object->getStatus()) {
            $data['status'] = $object->getStatus();
        }

        $data['violations'] = [];
        foreach ($object->getErrors() as $apiError) {
            $data['violations'][] = $this->normalizeError($apiError);
        }

        return $data;
    }

    private function normalizeError(ApiError $apiError): array
    {
        $violationEntry = [
            'propertyPath' => $apiError->getPropertyPath(),
            'title' => $this->translate($apiError->getTitle()),
        ];
        if (null !== $type = $apiError->getCode()) {
            $violationEntry['code'] = $type;
        }

        return $violationEntry;
    }

    private function translate(Message $message): string
    {
        return $transMessage = $this->translator->trans(
            $message->getMessageTemplate(),
            $message->getParameters(),
            $message->getDomain()
        );
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof ApiProblem;
    }
}
