<?php

namespace App\Api\Repository;

use App\Api\Entity\Basket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Basket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Basket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Basket[]    findAll()
 * @method Basket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Basket::class);
    }

    public function save(Basket $basket): void
    {
        if (!$basket->getId()) {
            $this->getEntityManager()->persist($basket);
        }

        $this->getEntityManager()->flush();
    }

    public function remove(Basket $basket): void
    {
        $this->getEntityManager()->remove($basket);
        $this->getEntityManager()->flush();
    }
}
