<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\Http\CreateOrUpdateBasketRequest;
use App\Api\Entity\Basket;
use App\Api\Repository\BasketRepository;
use App\Api\Service\BasketManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Route("/baskets")
 *
 * @SWG\Tag(name="Baskets")
 */
class BasketController extends AbstractApiController
{
    /**
     * @Route("", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @Model(type=CreateOrUpdateBasketRequest::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response="201",
     *     description="",
     *     @Model(type=Basket::class, groups={"api"}))
     * ),
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     *
     * @param CreateOrUpdateBasketRequest $createBasketRequest
     * @param BasketManager               $manager
     *
     * @return JsonResponse
     */
    public function create(CreateOrUpdateBasketRequest $createBasketRequest, BasketManager $manager)
    {
        $basket = $manager->createBasketByData($createBasketRequest);

        return $this->serialize($basket, Response::HTTP_CREATED);
    }

    /**
     * @Route("/{basketId}", methods={"PUT"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="JSON Payload",
     *     required=true,
     *     format="application/json",
     *     @Model(type=CreateOrUpdateBasketRequest::class, groups={"api"}),
     * ),
     *
     * @SWG\Response(
     *     response="200",
     *     description="If successfully updated",
     *     @Model(type=Basket::class, groups={"api"}))
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Basket not found",
     * ),
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/ConstraintViolationList"),
     * ),
     *
     * @param Basket                      $basket
     * @param CreateOrUpdateBasketRequest $updateBasketRequest
     * @param BasketManager               $manager
     *
     * @return JsonResponse
     */
    public function update(Basket $basket, CreateOrUpdateBasketRequest $updateBasketRequest, BasketManager $manager)
    {
        $basket = $manager->updateBasketByData($basket, $updateBasketRequest);

        return $this->serialize($basket);
    }

    /**
     * @Route("/{basketId}", methods={"GET"})
     *
     * @SWG\Response(
     *     response="200",
     *     description="",
     *     @Model(type=Basket::class, groups={"api"}))
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Basket not found",
     * ),
     *
     * @param Basket $basket
     *
     * @return JsonResponse
     */
    public function find(Basket $basket)
    {
        return $this->serialize($basket);
    }

    /**
     * @todo paging
     *
     * @Route("", methods={"GET"})
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns baskets",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Basket::class, groups={"list"}))
     *     )
     * ),
     *
     * @param BasketRepository $repository
     *
     * @return JsonResponse
     */
    public function list(BasketRepository $repository)
    {
        $baskets = $repository->findAll();

        return $this->serialize($baskets, Response::HTTP_OK, ['groups' => self::RESPONSE_GROUP_LIST]);
    }

    /**
     * @Route("/{basketId}", methods={"DELETE"})
     *
     * @SWG\Response(
     *     response="200",
     *     description="If successfully deleted"
     * ),
     * @SWG\Response(
     *     response="404",
     *     description="Basket not found",
     * ),
     *
     * @param Basket           $basket
     * @param BasketRepository $repository
     *
     * @return JsonResponse
     */
    public function remove(Basket $basket, BasketRepository $repository)
    {
        $repository->remove($basket);

        return $this->serialize(null);
    }
}
