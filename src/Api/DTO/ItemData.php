<?php

declare(strict_types=1);

namespace App\Api\DTO;

use App\Api\Enum\ItemType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

class ItemData
{
    /**
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"\App\Api\Enum\ItemType", "toArray"})
     *
     * @Groups({"api", "list"})
     *
     * @var string
     */
    private string $type;
    /**
     * @Assert\NotBlank()
     * @Assert\Range(min="1", max="1000")
     *
     * @Groups({"api", "list"})
     *
     * @var int
     */
    private int $weight;

    public function __construct(string $type, int $weight)
    {
        $this->type = $type;
        $this->weight = $weight;
    }

    public function getType(): ItemType
    {
        return new ItemType($this->type);
    }

    public function getWeight(): int
    {
        return $this->weight;
    }
}
