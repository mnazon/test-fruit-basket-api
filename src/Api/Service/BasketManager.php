<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\Http\AddItemsToBasketRequest;
use App\Api\DTO\Http\CreateOrUpdateBasketRequest;
use App\Api\DTO\ItemData;
use App\Api\Entity\Basket;
use App\Api\Entity\Item;
use App\Api\Repository\BasketRepository;

class BasketManager
{
    private BasketRepository $repository;

    public function __construct(BasketRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createBasketByData(CreateOrUpdateBasketRequest $basketData): Basket
    {
        $basket = new Basket(
            $basketData->getName(),
            $basketData->getMaxWeight()
        );

        $this->repository->save($basket);

        return $basket;
    }

    public function updateBasketByData(Basket $basket, CreateOrUpdateBasketRequest $basketData): Basket
    {
        $basket
            ->setName($basketData->getName())
            ->setMaxWeight($basketData->getMaxWeight())
        ;

        $this->repository->save($basket);

        return $basket;
    }

    /**
     * @param Basket                  $basket
     * @param AddItemsToBasketRequest $request
     *
     * @return Item[]
     */
    public function addItemsToBasket(Basket $basket, AddItemsToBasketRequest $request): array
    {
        $items = [];
        /** @var ItemData $itemData */
        foreach ($request->getItems() as $itemData) {
            /** @var Item $item */
            $items[] = $item = $this->createItem($basket, $itemData);
            $basket->addItem($item);
        }

        $this->repository->save($basket);

        return $items;
    }

    private function createItem(Basket $basket, ItemData $itemData): Item
    {
        return new Item(
            $basket,
            $itemData->getType(),
            $itemData->getWeight()
        );
    }
}
