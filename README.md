Run application
```
docker-compose up -d
```

Build app
```
docker-compose run --rm --no-deps php-fpm sh -lc 'composer install'
```

Build migrations
```
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate --env=test
docker-compose exec php-fpm php bin/console doctrine:migrations:migrate --env=dev
```

Run tests
```
docker-compose exec php-fpm ./vendor/bin/phpunit
```

Load fixtures
```
docker-compose exec php-fpm php bin/console doctrine:fixtures:load --append
```

Doc for api available on
```
http://localhost:11202/api/doc
```