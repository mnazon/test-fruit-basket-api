<?php

namespace App\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Api\Repository\BasketRepository")
 * @ORM\Table(name="basket")
 */
class Basket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"api", "list"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"api", "list"})
     */
    private string $name;

    /**
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     *
     * @Groups({"api", "list"})
     */
    private int $maxWeight;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Api\Entity\Item",
     *     mappedBy="basket",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     *
     * @Groups({"api"})
     */
    private Collection $items;

    public function __construct(string $name, int $maxWeight)
    {
        $this->name = $name;
        $this->maxWeight = $maxWeight;
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaxWeight(): int
    {
        return $this->maxWeight;
    }

    public function setMaxWeight(int $maxWeight): self
    {
        $this->maxWeight = $maxWeight;

        return $this;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
        }

        return $this;
    }

    public function getWeight(): int
    {
        $weight = 0;
        foreach ($this->getItems() as $item) {
            $weight += $item->getWeight();
        }

        return $weight;
    }
}
