<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use Symfony\Component\Serializer\Normalizer\ConstraintViolationListNormalizer as CoreConstraintViolationListNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintViolationListNormalizer implements NormalizerInterface
{
    private NormalizerInterface $normalizer;

    public function __construct(CoreConstraintViolationListNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        foreach ($object as $key => $violation) {
            /** @var $violation ConstraintViolationInterface */
            if (null !== $code = $violation->getCode()) {
                if (isset($data['violations'][$key]) && !isset($data['violations'][$key]['code'])) {
                    $data['violations'][$key]['code'] = $code;
                }
            }
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof ConstraintViolationListInterface;
    }
}
