<?php

declare(strict_types=1);

namespace App\Api\DTO\Http;

use App\Api\DTO\ArgumentResolvableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

class RemoveItemsFromBasketRequest implements ArgumentResolvableInterface
{
    /**
     * @Assert\NotNull()
     *
     * @Groups("api")
     *
     * @var int[]
     */
    private array $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
