<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\DataFixtures\AppFixtures;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\ConsoleOutput;

class BaseApiTestCase extends WebTestCase
{
    public static array $defaultHeaders = [
        'CONTENT_TYPE' => 'application/json',
    ];

    protected KernelBrowser $client;

    private Loader $fixtureLoader;

    private ?ResponseAsserter $responseAsserter = null;

    public function setUp(): void
    {
        $this->client = $this->createClient([], self::$defaultHeaders);

        parent::setUp();
    }

    protected function loadFixtures(array $fixtures, bool $append = false): void
    {
        $this->fixtureLoader = new Loader();
        $entityManager = self::$container->get('doctrine')->getManager();
        $ormExecutor = new ORMExecutor($entityManager, new ORMPurger($entityManager));

        $consoleLogger = new ConsoleLogger(new ConsoleOutput(ConsoleOutput::VERBOSITY_DEBUG));
        $ormExecutor->setLogger(function ($message) use ($consoleLogger) {
            $consoleLogger->info($message);
        });
        foreach ($fixtures as $fixture) {
            $this->fixtureLoader->addFixture($fixture);
        }

        $ormExecutor->execute($this->fixtureLoader->getFixtures(), $append);
    }

    protected function getResponseAsserter(): ResponseAsserter
    {
        if (is_null($this->responseAsserter)) {
            $this->responseAsserter = new ResponseAsserter($this);
        }

        return $this->responseAsserter;
    }

    protected function prepareCollectionStructure(array $singleStructure, int $iterateCount): array
    {
        $exceptedProperties = [];
        for ($i = 0; $i < $iterateCount; ++$i) {
            foreach ($singleStructure as $item) {
                $exceptedProperties[] = "[{$i}].$item";
            }
        }

        return $exceptedProperties;
    }

    protected function loadAppFixtures(bool $append = false): AppFixtures
    {
        $fixtures = new AppFixtures();
        $this->loadFixtures([$fixtures], $append);

        return $fixtures;
    }
}
