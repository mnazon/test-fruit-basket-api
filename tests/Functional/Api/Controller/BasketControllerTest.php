<?php

declare(strict_types=1);

namespace App\Tests\Functional\Api\Controller;

use App\Tests\Functional\BaseApiTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @todo bad request tests
 */
class BasketControllerTest extends BaseApiTestCase
{
    private const BASE_URL = '/api/v1/baskets';
    private const GET_BASKET_FORMAT = self::BASE_URL.'/%s';

    private const BASKET_LIST_GROUP_STRUCTURE = [
        'id',
        'name',
        'maxWeight',
    ];
    private const BASKET_API_GROUP_STRUCTURE = self::BASKET_LIST_GROUP_STRUCTURE + ['items'];

    public function testSuccessCreateBasket()
    {
        $data = [
            'name' => 'test',
            'maxWeight' => 100,
        ];

        $this->client->request('POST', self::BASE_URL, [], [], self::$defaultHeaders, json_encode($data));
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_CREATED, $response)
            ->assertResponsePropertiesExist($response, self::BASKET_API_GROUP_STRUCTURE);
    }

    public function testSuccessUpdateBasket()
    {
        $name = 'testnew';
        $maxWeight = 1000;

        $data = [
            'name' => $name,
            'maxWeight' => $maxWeight,
        ];

        $basket = $this->loadAppFixtures()->getBasket();
        $url = sprintf(self::GET_BASKET_FORMAT, $basket->getId());
        $this->client->request('PUT', $url, [], [], self::$defaultHeaders, json_encode($data));
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusOk($response)
            ->assertResponsePropertyEquals($response, 'name', $name)
            ->assertResponsePropertyEquals($response, 'maxWeight', $maxWeight);
    }

    public function testUpdateNoExistingBasket()
    {
        $data = [
            'name' => 'testnew',
            'maxWeight' => 1000,
        ];

        $notExistingBasketId = 0;

        $url = sprintf(self::GET_BASKET_FORMAT, $notExistingBasketId);
        $this->client->request('PUT', $url, [], [], self::$defaultHeaders, json_encode($data));
        $response = $this->client->getResponse();

        $this->getResponseAsserter()->assertStatusEquals(Response::HTTP_NOT_FOUND, $response);
    }

    public function testSuccessFindBasket()
    {
        $basket = $this->loadAppFixtures()->getBasket();
        $this->client->request('GET', self::BASE_URL.'/'.$basket->getId());
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusOk($response)
            ->assertResponsePropertiesExist($response, self::BASKET_API_GROUP_STRUCTURE);
    }

    public function testFindNoExistingBasket()
    {
        $notExistingBasketId = 0;
        $this->client->request('GET', sprintf(self::GET_BASKET_FORMAT, $notExistingBasketId));
        $response = $this->client->getResponse();

        $this->getResponseAsserter()->assertStatusEquals(Response::HTTP_NOT_FOUND, $response);
    }

    public function testSuccessGetBaskets()
    {
        $this->loadAppFixtures();
        $this->client->request('GET', self::BASE_URL);
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusOk($response)
            ->assertResponsePropertiesExist(
                $response,
                $this->prepareCollectionStructure(self::BASKET_LIST_GROUP_STRUCTURE, 1)
            );
    }

    public function testSuccessRemoveBasket()
    {
        $basket = $this->loadAppFixtures()->getBasket();
        $url = sprintf(self::GET_BASKET_FORMAT, $basket->getId());
        $this->client->request('DELETE', $url);
        $response = $this->client->getResponse();

        $this->getResponseAsserter()
            ->assertStatusEquals(Response::HTTP_OK, $response)
            ->assertResponseContentEquals($response, 'null');

        $this->client->request('GET', $url);
        $response = $this->client->getResponse();

        $this->getResponseAsserter()->assertStatusEquals(Response::HTTP_NOT_FOUND, $response);
    }
}
